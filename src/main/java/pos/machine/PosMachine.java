package pos.machine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        Map<String, Integer> itemAndNum = getItemAndNum(barcodes);
        String tmpReceipt = receipt(itemAndNum);
        String result = finalReceipt(tmpReceipt);
        return result;
    }

    public Map<String, Integer> getItemAndNum(List<String> barcodes){
        Map<String, Integer> itemMap = new HashMap();
        for (String barcode : barcodes) {
            if(!itemMap.containsKey(barcode)){
                itemMap.put(barcode,1);
            }else {
                Integer num = itemMap.get(barcode);
                itemMap.put(barcode, num+1);
            }
        }
        return itemMap;
    }

    public String line(Map.Entry<String, Integer> entry, List<Item> items){
        String line = null;
        for (Item item : items) {
            if(item.getBarcode().equals(entry.getKey())){
                line ="Name: " + item.getName() +
                        ", Quantity: " + entry.getValue() +
                        ", Unit price: "+ item.getPrice() +
                        " (yuan), Subtotal: " + item.getPrice()* entry.getValue() + "(yuan)";
            }
        }
        return line;
    }

    public String receipt(Map<String, Integer> itemAndNum) {
        StringBuffer receipt = new StringBuffer();

        List<Item> items = ItemsLoader.loadAllItems();
        for (Map.Entry<String, Integer> entry : itemAndNum.entrySet()) {
            receipt.append(line(entry, items)).append("\n") ;
        }
        return  receipt.toString();
    }

    public String finalReceipt(String tmpReceipt){
        StringBuffer sb = new StringBuffer();
        sb.append("***<store earning no money>Receipt>***\n");
        sb.append(tmpReceipt);
        sb.append("----------------------\n").append("Total: 24 (yuan)\n" + "**********************\n");
        return sb.toString();
    }
}
